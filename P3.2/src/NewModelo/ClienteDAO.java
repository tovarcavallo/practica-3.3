/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewModelo;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author carlo
 */
public interface ClienteDAO {
    public List<Cliente> listarClientes(int desde, int cuanto);
    public boolean insertCliente(String nombre, String apellido, String direccion, String telefono) throws SQLException;
    public Cliente updateCliente(Cliente cliente);
    public boolean deleteCliente(int idCliente);
    public Cliente getCliente(int idCliente);
    public List<Cliente> getClienteLike(String nombre);
    public List<Cliente> getClienteWhere(String direccion);
    
}
