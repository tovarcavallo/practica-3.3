/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewModelo;

import java.util.List;

/**
 *
 * @author carlo
 */
public interface Cliente_EmpleadoDAO {
    public List<Cliente_Empleado> listarCliente_Empleado(int desde, int cuanto);
    public boolean insertCliente_Empleado(int idCliente,int idEmpleado);
    public Cliente_Empleado updateCliente_Empleado(Cliente_Empleado cliemple);
    public boolean deleteCliente_Empleado(int codigo);
    public Cliente_Empleado getCliente_Empleado(int codigo);
    public List<Cliente_Empleado> getCliente(int idCliente);
    public List<Cliente_Empleado> getEmpleado(int idEmpleado);
    
    
}
