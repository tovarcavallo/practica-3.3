/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;


import NewModelo.Empleado;
import NewModelo.EmpleadoDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carlo
 */
public class EmpleadoController implements EmpleadoDAO {
     Connection con;
 
 private static final String SELECT_ALL_EMPLEADOS_PAGINATION = "Select * from empleado order by id limit ? offset ?";
 private static final String INSERT_EMPLEADO_QUERY="insert into empleado (nombre,apellido,direccion,telefono) values";
 private static final String SELECT_LAST_ID = "select * from empleado order by id";
 private static final String DELETE_EMPLEADO = "delete from empleado where id = ?";
 private static final String SELECT_EMPLEADO = "select * from empleado where id = ?" ;         
 private static final String UPDATE_EMPLEADO = "update empleado SET nombre = ?, apellido = ?, direccion = ?, telefono = ?  where id = ?";
 private static final String SELECT_EMPLEADO_LIKE = "select * from empleado where nombre = ";
 private static final String SELECT_EMPLEADO_WHERE = "select * from empleado where direccion = ";
 public EmpleadoController(Connection con){
     super();
     this.con=con;
 }

    @Override
    public List<Empleado> listarEmpleado(int desde, int cuanto) { // devuelve lista de empleados
                PreparedStatement st; 
        List<Empleado> empleados = new ArrayList<>();
        try{
            st = con.prepareStatement(SELECT_ALL_EMPLEADOS_PAGINATION);
            st.setInt(1, cuanto);
            st.setInt(2, desde);
            ResultSet rs=  st.executeQuery();
            
            while(rs.next()){
                int idE = rs.getInt("id");
                String nombreC = rs.getString("nombre");
                String apellidoC = rs.getString("apellido");
                String direccionC = rs.getString("direccion");
                String telefonoC = rs.getString("telefono");
                
                Empleado emp = new Empleado(idE,nombreC,apellidoC,direccionC,telefonoC);
                empleados.add(emp);
            }
        } catch (SQLException ex) {
         ex.printStackTrace();
         empleados = null;
     }
        return empleados;
    }

    @Override
    public boolean insertEmpleado(String nombre, String apellido, String direccion, String telefono) { //Para insertar empleados al Restaurante, devuelve un boolean que valdrá true si lo logra y false si no pudo lograrlo
                boolean u = false;
        Statement st=null;
        int num;
        try{

            st=con.createStatement();
            //Inserto
          num = st.executeUpdate(INSERT_EMPLEADO_QUERY + " ("+"\""+nombre+"\""+","+"\""+apellido+"\""+","+"\""+direccion+"\""+","+"\""+telefono+"\""+")"); 
          
          if (num==1){
              u=true;
          }else{
              System.err.println("Error: No se ha podido insertar Empleado");
              u = false;
          }
          } catch (SQLException sqle) {
            System.err.println(sqle.getMessage());
          }
        finally{
            try{
                if (st == null)
                    st.close();

                
            }catch (SQLException sqle) {
                sqle.printStackTrace();  
            }
        }
            
        return u;
        
        
    }

    @Override
    public Empleado updateEmpleado(Empleado empleado) { //Para cambiar datos de empleado, devuelve empleado acualizado
             Empleado emp = null;
       int id = empleado.getId();
       emp = getEmpleado(id);
       if (emp!=null){
           if (empleado.getNombre().equals(emp.getNombre()) && empleado.getApellido().equals(emp.getApellido()) && empleado.getDireccion().equals(emp.getDireccion()) && empleado.getTelefono().equals(emp.getTelefono())){
               System.out.println("No se han observado cambios");
           }else{
               PreparedStatement ps;
               try{
                   ps = con.prepareStatement(UPDATE_EMPLEADO);
                   ps.setString(1, empleado.getNombre());
                   ps.setString(2, empleado.getApellido());
                   ps.setString(3, empleado.getDireccion());
                   ps.setString(4, empleado.getTelefono());
                   ps.setInt(5, empleado.getId());
                   
                   int aux = ps.executeUpdate();
                   
                   if (aux==1){
                       emp= empleado;
                   }else{
                       emp=null;
                   }
               } catch (SQLException ex) {
                   ex.printStackTrace();
               }
           }
       }else
            System.err.println("Error: No existe un Empleado con ese id");
       
       return emp;
    }

    @Override
    public boolean deleteEmpleado(int idEmpleado) { //Para eliminar empleado, devulve boolean que valdrá true si lo logra, y false si no lo logra
               Empleado emp = getEmpleado(idEmpleado);
         boolean u = false;
         
         if (emp!=null){
             PreparedStatement st = null;
             
             try{
                 st = con.prepareStatement(DELETE_EMPLEADO);
                 st.setInt(1, idEmpleado);
                 int num = st.executeUpdate();
                   if (num == 1){
                       u = true;
                   }else{
                       u=false;
                   }
             } catch (SQLException ex) {
                 ex.printStackTrace();
             }
             }else{
             System.err.println("Error: No existe un empleado con el id ingresado");
         }
         return u;
    }

    @Override
    public Empleado getEmpleado(int idEmpleado) { //Devuelve empleado con id ingresado por el usuario
         PreparedStatement st;
        Empleado emp = null;
        try{
            st = con.prepareStatement(SELECT_EMPLEADO);
            st.setInt(1, idEmpleado);
            ResultSet rs = st.executeQuery();
            
            if (rs.next()){
                int idE = rs.getInt("id");
                String nombreE = rs.getString("nombre");
                String apellidoE = rs.getString("apellido");
                String direccionE = rs.getString("direccion");
                String telefonoE = rs.getString("telefono");
                
                emp = new Empleado (idE,nombreE,apellidoE,direccionE,telefonoE);
            }else{
                System.err.println("Error: No existe un empleado con el id ingresado");
                
            }
        } catch (SQLException ex) {
        ex.printStackTrace();
     }
        return emp;
    }

    @Override
    public List<Empleado> getEmpleadoLike(String nombre) { //Devuelve una lista con todos los empleados con el nombre que el usuario indique
              PreparedStatement st; 
        List<Empleado> empleados = null;
          try{
              empleados= new ArrayList<>();
              st = con.prepareStatement(SELECT_EMPLEADO_LIKE +"\""+ nombre+"\"");
              ResultSet rs=  st.executeQuery();
              
              while (rs.next()){
                int idE = rs.getInt("id");
                String nombreE = rs.getString("nombre");
                String apellidoE = rs.getString("apellido");
                String direccionE = rs.getString("direccion");
                String telefonoE = rs.getString("telefono");
                
                Empleado emp = new Empleado(idE,nombreE,apellidoE,direccionE,telefonoE);
                empleados.add(emp);
              }
                if (empleados.isEmpty()){  //Si no hay nada en la lista, es porque no hay ninguna coincidencia
                  System.err.println("Error: El nombre ingresado no se encuentra como Empleado");
                  
              }
              
          } catch (SQLException ex) {
        ex.printStackTrace();
        empleados = null;
          }

            return empleados;
    }

    @Override
    public List<Empleado> getEmpleadoWhere(String direccion) { //Devuelve una lista con todos los empleados con la direccion que el usuario indique
                 PreparedStatement st; 
        List<Empleado> empleados = null;
          try{
              empleados= new ArrayList<>();
              st = con.prepareStatement(SELECT_EMPLEADO_WHERE +"\""+ direccion+"\"");
              ResultSet rs=  st.executeQuery();
              
              while (rs.next()){
                int idE = rs.getInt("id");
                String nombreE = rs.getString("nombre");
                String apellidoE = rs.getString("apellido");
                String direccionE = rs.getString("direccion");
                String telefonoE = rs.getString("telefono");
                
                Empleado emp = new Empleado(idE,nombreE,apellidoE,direccionE,telefonoE);
                empleados.add(emp);
              }
                if (empleados.isEmpty()){  //Si no hay nada en la lista, es porque no hay ninguna coincidencia
                  System.err.println("Error: La direccion ingresada no se encuentra como Empleado");
                          }
          } catch (SQLException ex) {
        ex.printStackTrace();
        empleados = null;
          }

            return empleados;
    }
    
}
