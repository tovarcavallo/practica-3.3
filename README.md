Proyecto que contiene la Practica 3.3 y la Base de Datos "Restaurante".
Informar que, en comparación con las otras practicas, se tuvo que modificar la BBDD en la tabla Cliente_Empleado, agregando una variable 
"codigo" que servira como id para cada dato.
Y al mismo tiempo se modifico la clase Cliente_Empleado para la agregacion de ésta misma variable.
Ademas, se elimino la funcion getCliente_EmpleadoLike de Cliente_EmpleadoDAO, ya que realizaba la misma funcion que getCliente o getEmpleado de esta misma clase.